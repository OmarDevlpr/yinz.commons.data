import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import Model from "../../../main/ts/Model";
import { ClassRoomModel } from "./ClassRoomModel";

@Entity()
export class TeacherModel extends Model {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string;



    /* AUDIT TRAIL FIELDS*/
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;

    @OneToMany(type => ClassRoomModel, classRoom => classRoom.teacher)
    classes: ClassRoomModel[];

}
