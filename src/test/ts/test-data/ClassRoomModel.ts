import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import Model from "../../../main/ts/Model";
import { TeacherModel } from "./TeacherModel";

@Entity()
export class ClassRoomModel extends Model {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string;



    /* AUDIT TRAIL FIELDS*/
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;

    @ManyToOne(type => TeacherModel)
    teacher: TeacherModel;
    
}
