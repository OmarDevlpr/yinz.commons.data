import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import Model from "../../../main/ts/Model";

@Entity()
export class SampleModel extends Model {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ nullable: true})
    name: string;

    @Column({ nullable: true})
    rank: number;


    /* AUDIT TRAIL FIELDS*/
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;

    // @OneToMany( type => I18nSampleModel, i18nSampleModel => i18nSampleModel.locale)
    // locale: string;

}
