import { Column, Entity, TableInheritance, ChildEntity, OneToOne, JoinColumn } from "typeorm";
import Model from "../../../main/ts/Model";
import LogModel from "../../../main/ts/LogModel";

@Entity()
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export class Salary extends Model {    
    @Column()
    amount: number;

    @Column({nullable: true})
    ref: string;
}


@ChildEntity()
export class StaffSalary extends Salary {}


@ChildEntity()
export class ManagerSalary extends Salary {
    @Column({nullable: true})
    descr: string;
}


@Entity()
export class SalaryPayementLog extends LogModel {
    @Column({ nullable: true })
    descr?: string;
    @Column({ nullable: true })
    amount?: number;

    @OneToOne(type => Salary)
    @JoinColumn()
    salary: Salary
}