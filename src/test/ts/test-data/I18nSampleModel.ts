import { Column, Entity } from "typeorm";
import Model from "../../../main/ts/Model";

@Entity()
export class I18nSampleModel extends Model {

    @Column()
    locale: string;

    @Column()
    name: string;    

    @Column()
    ownerId: number;    
}
