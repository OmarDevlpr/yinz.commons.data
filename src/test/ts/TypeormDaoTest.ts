// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "../../main/ts/DataSource";
import { TypeormDao, TypeormDataSource } from "../../main/";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { SampleModel } from "./test-data/SampleModel";
import { TeacherModel } from "./test-data/TeacherModel";
import { ClassRoomModel } from "./test-data/ClassRoomModel";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


const sampleModelDao = new TypeormDao<SampleModel>({
    model: SampleModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('SampleModel', SampleModel);
container.setBean('sampleModelDao', sampleModelDao);

const teacherModelDao = new TypeormDao<TeacherModel>({
    model: TeacherModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('TeacherModel', TeacherModel);
container.setBean('teacherModelDao', teacherModelDao);

const classRoomModelDao = new TypeormDao<ClassRoomModel>({
    model: ClassRoomModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('ClassRoomModel', ClassRoomModel);
container.setBean('classRoomModelDao', classRoomModelDao);


describe('| commons.data.TypeormDao', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)                
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });



    it('| create --> should create an instance of sample model', async () => {
        // 1. prepare test data

        let sampleModelDto = new SampleModel();
        sampleModelDto.name = chance.string({length: 10});

        let options = { user: "__super_user__" };

        // 2. execute test

        let result = await sampleModelDao.create(trans, sampleModelDto, options);

        // 3. compare result

        assert.equal(result.name, sampleModelDto.name);
        assert.ok(result.id);
        assert.ok(result.createdOn);
        assert.ok(result.createdBy);
    });


    it('| create --> should create an instance of classRoom', async () => {
        // 1. prepare test data

        let classRoomModelDto = new ClassRoomModel();
        classRoomModelDto.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        // 2. execute test

        let result = await classRoomModelDao.create(trans, classRoomModelDto, options);

        // 3. compare result

        assert.equal(result.name, classRoomModelDto.name);
        assert.ok(result.id);
        assert.ok(result.createdOn);
        assert.ok(result.createdBy);

    });

    it('| create --> should create an instance of teacher with class association', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let notREgisteredClsas = new ClassRoomModel();
        notREgisteredClsas.name = chance.string({ length: 10 });
        notREgisteredClsas.id = 1551;

        let notREgisteredClsas2 = new ClassRoomModel();
        notREgisteredClsas2.name = chance.string({ length: 10 });
        notREgisteredClsas2.id = 1552;

        let teacherModelDto = new TeacherModel();
        teacherModelDto.name = chance.string({ length: 10 });


        let options = { user: "__super_user__" };

        // 2. execute test

        let resultClass = await classRoomModelDao.create(trans, simpleClass, options);
        teacherModelDto.classes = [resultClass /*, notREgisteredClsas, notREgisteredClsas2*/]

        let resultTeacher = await teacherModelDao.create(trans, teacherModelDto, { ...options, include: ['classes']});

        let findOptions = { ...options, include: ['classes'] }
        let result = await teacherModelDao.findById(trans, resultTeacher.id, findOptions);


        // 3. compare result        

        assert.equal(result.name, teacherModelDto.name);
        assert.equal(result.classes[0].name, simpleClass.name)
        assert.ok(result.createdOn);
        assert.ok(result.createdBy);
    });


    it('| create --> should throw an error as the association does not contain an id', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModelDto = new TeacherModel();
        teacherModelDto.name = chance.string({ length: 10 });
        teacherModelDto.classes = [simpleClass]

        let options = { user: "__super_user__" };

        try {

            // 2. execute test

            let resultTeacher = await teacherModelDao.create(trans, teacherModelDto, options);

            let findOptions = { ...options, include: ['classes'] }
            let result = await teacherModelDao.findById(trans, resultTeacher.id, findOptions);


            // 3. compare result        

            assert.equal(result.name, teacherModelDto.name);
            assert.ok(result.createdOn);
            assert.ok(result.createdBy);
        } catch (e) {

            if (e instanceof Exception) {

                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS")
                assert.strictEqual(e.message, "[TeacherModel] association [classes] is missing the [id] field.")


            }
            else {
                throw new Error('BAD FLOW... we should not be here!');
            }

        }

    });


    it('| create --> should throw an error as the association exist', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });
        simpleClass.id = 5555;

        let teacherModelDto = new TeacherModel();
        teacherModelDto.name = chance.string({ length: 10 });
        teacherModelDto.classes = [simpleClass]

        let options = { user: "__super_user__" };

        try {

            // 2. execute test

            let resultTeacher = await teacherModelDao.create(trans, teacherModelDto, options);

            let findOptions = { ...options, include: ['classes'] }
            let result = await teacherModelDao.findById(trans, resultTeacher.id, findOptions);


            // 3. compare result        

            assert.equal(result.name, teacherModelDto.name);
            assert.ok(result.createdOn);
            assert.ok(result.createdBy);
        } catch (e) {

            if (e instanceof Exception) {

                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_EXISTS")
                assert.strictEqual(e.message, "no [ClassRoomModel] associations with ids [5555] exist!")


            }
            else {
                throw new Error('BAD FLOW... we should not be here!');
            }

        }

    });

    it('| create --> Should create a teacher with class, when fetching class, teacher has to exist', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModelDto = new TeacherModel();
        teacherModelDto.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {

            // 2. execute test
            let classRoomCreateResult = await classRoomModelDao.create(trans, simpleClass, options);
            teacherModelDto.classes = [classRoomCreateResult];
            await teacherModelDao.create(trans, teacherModelDto, {...options, include: ['classes']});


            let findOptions = { ...options, include: ['teacher'] }
            let result = await classRoomModelDao.findById(trans, classRoomCreateResult.id, findOptions);
            // 3. compare result        

            assert.equal(result.name, simpleClass.name);
            assert.equal(result.teacher.name, teacherModelDto.name);
            assert.ok(result.createdOn);
            assert.ok(result.createdBy);
        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| create --> Creating two teachers with same class should override the class teacher and first teacher should not have any classes', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModelDto = new TeacherModel();
        teacherModelDto.name = chance.string({ length: 10 });

        let teacherModelDto2 = new TeacherModel();
        teacherModelDto2.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {

            // 2. execute test
            let classRoomCreateResult = await classRoomModelDao.create(trans, simpleClass, options);
            teacherModelDto.classes = [classRoomCreateResult];
            teacherModelDto2.classes = [classRoomCreateResult];

            let teacher1Result1 = await teacherModelDao.create(trans, teacherModelDto, {...options, include: ['classes']});
            let teacher1Result2 = await teacherModelDao.create(trans, teacherModelDto2, { ...options, include: ['classes'] });


            let classfindOptions = { ...options, include: ['teacher'] }
            let teacherFindOptions = { ...options, include: ['classes'] }

            let result = await classRoomModelDao.findById(trans, classRoomCreateResult.id, classfindOptions);
            let teacher1FindResult = await teacherModelDao.findById(trans, teacher1Result1.id, teacherFindOptions);
            let teacher2FindResult = await teacherModelDao.findById(trans, teacher1Result2.id, teacherFindOptions);

            // 3. compare result        

            assert.equal(result.name, simpleClass.name);
            assert.equal(result.teacher.name, teacherModelDto2.name);
            assert.equal(teacher1FindResult.classes.length, 0);
            assert.equal(teacher2FindResult.classes.length, 1);
            assert.ok(result.createdOn);
            assert.ok(result.createdBy);
        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| create --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        try {

            // 2. execute test
            await classRoomModelDao.create(trans, simpleClass);                     
        } catch (e) {

            if ( e instanceof Exception ) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }            
        }

    });





    it('| createAll --> should createAll 3 instances of sample model', async () => {
        // 1. prepare test data

        let sampleModelDto1 = new SampleModel();
        let sampleModelDto2 = new SampleModel();
        let sampleModelDto3 = new SampleModel();
        sampleModelDto1.name = chance.string({ length: 10 });
        sampleModelDto2.name = chance.string({ length: 10 });
        sampleModelDto3.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };
        let objects = [sampleModelDto1, sampleModelDto2, sampleModelDto3];

        // 2. execute test

        let result = await sampleModelDao.createAll(trans, objects, options);

        let findAllResult = await sampleModelDao.findAll(trans, options);
        if (findAllResult) {

        }
        // 3. compare result, create All does not guarantee the order of creation, hence we only check we have exactly three instances with id.
        assert.ok(result[0].id);
        assert.ok(result[1].id);
        assert.ok(result[2].id);

    });

    it('| createAll --> should createAll 3 instances of teacher model with the same class, only last teacher should have class left. ', async () => {
        // 1. prepare test data

        let classModelDto = new ClassRoomModel();
        classModelDto.name = chance.string({ length: 10 });

        let teacherModelDto1 = new TeacherModel();
        let teacherModelDto2 = new TeacherModel();
        let teacherModelDto3 = new TeacherModel();
        teacherModelDto1.name = chance.string({ length: 10 });       
        teacherModelDto2.name = chance.string({ length: 10 });
        teacherModelDto3.name = chance.string({ length: 10 });
        

        
        let options = { user: "__super_user__" };
        let objects = [teacherModelDto1, teacherModelDto2, teacherModelDto3];

        // 2. execute test

        classModelDto = await classRoomModelDao.create(trans, classModelDto, options);

        teacherModelDto1.classes = [classModelDto];
        teacherModelDto2.classes = [classModelDto];
        teacherModelDto3.classes = [classModelDto];
        
        await teacherModelDao.createAll(trans, objects, { ...options, include: ['classes'] });

        let findOptions = { ...options, include: ['classes'] }
        let findAllResult = await teacherModelDao.findAll(trans, findOptions);        
        // 3. compare result, create all does not ensure the order, hence we gonna check that we have at least one instance with class

        assert.ok(findAllResult[0].classes[0] || findAllResult[1].classes[0] || findAllResult[2].classes[0]);
        
    });

    it('| createAll --> should throw an error as association does not have an id ', async () => {
        // 1. prepare test data

        let classModelDto = new ClassRoomModel();
        classModelDto.name = chance.string({ length: 10 });

        let teacherModelDto1 = new TeacherModel();
        let teacherModelDto2 = new TeacherModel();
        let teacherModelDto3 = new TeacherModel();
        teacherModelDto1.name = chance.string({ length: 10 });
        teacherModelDto2.name = chance.string({ length: 10 });
        teacherModelDto3.name = chance.string({ length: 10 });



        let options = { user: "__super_user__" };
        let objects = [teacherModelDto1, teacherModelDto2, teacherModelDto3];

        // 2. execute test        

        teacherModelDto1.classes = [classModelDto];
        teacherModelDto2.classes = [classModelDto];
        teacherModelDto3.classes = [classModelDto];


        try {        
            await teacherModelDao.createAll(trans, objects, options);

            let findOptions = { ...options, include: ['classes'] }
            let findAllResult = await teacherModelDao.findAll(trans, findOptions);
            // 3. compare result

            assert.ok(findAllResult[0].classes[0].name === classModelDto.name);
            assert.ok(findAllResult[1].classes.length === 0);
            assert.ok(findAllResult[2].classes.length === 0);
        } catch (e) {

            if ( e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS')
                assert.strictEqual(e.message, '[TeacherModel] association [classes] is missing the [id] field.')
            } else {
                throw new Error("BAD FLOW... we should not be here!")
            }
        }

    });


    it('| createAll --> should throw an error as association does not exist in the database ', async () => {
        // 1. prepare test data

        let classModelDto = new ClassRoomModel();
        classModelDto.name = chance.string({ length: 10 });
        classModelDto.id = 5;

        let teacherModelDto1 = new TeacherModel();
        let teacherModelDto2 = new TeacherModel();
        let teacherModelDto3 = new TeacherModel();
        teacherModelDto1.name = chance.string({ length: 10 });
        teacherModelDto2.name = chance.string({ length: 10 });
        teacherModelDto3.name = chance.string({ length: 10 });



        let options = { user: "__super_user__" };
        let objects = [teacherModelDto1, teacherModelDto2, teacherModelDto3];

        // 2. execute test        

        teacherModelDto1.classes = [classModelDto];
        teacherModelDto2.classes = [classModelDto];
        teacherModelDto3.classes = [classModelDto];


        try {
            await teacherModelDao.createAll(trans, objects, options);

            let findOptions = { ...options, include: ['classes'] }
            let findAllResult = await teacherModelDao.findAll(trans, findOptions);
            // 3. compare result

            assert.ok(findAllResult[0].classes[0].name === classModelDto.name);
            assert.ok(findAllResult[1].classes.length === 0);
            assert.ok(findAllResult[2].classes.length === 0);
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_EXISTS')
                assert.strictEqual(e.message, 'no [ClassRoomModel] associations with ids [5] exist!')
            } else {
                throw new Error("BAD FLOW... we should not be here!")
            }
        }

    });


    it('| createAll --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        try {

            // 2. execute test
            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass]);
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });

    it('| updateByFilter --> should contains updated fields, lastUpdateBy and lastUpdateOn', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = {user: "__super_user__"};
        let updatedName = chance.string({ length: 10 });

        try {

            // 2. execute test
            await classRoomModelDao.create(trans, simpleClass, options);

            let updateResult = await classRoomModelDao.updateByFilter(trans, { name: updatedName }, { name: simpleClass.name}, options);

            assert.ok(updateResult)
            assert.ok(updateResult.data)                        
            assert.ok(updateResult.data.lastUpdateBy)            
            assert.ok(updateResult.data.lastUpdateOn)            
            assert.strictEqual(updateResult.data.name, updatedName)

        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });


    it('| updateByFilter --> should update all but one', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let simpleClassNoUpdate = new ClassRoomModel();
        simpleClassNoUpdate.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        try {

            // 2. execute test
            await classRoomModelDao.create(trans, simpleClassNoUpdate, options);
            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass, simpleClass], options);

            let updateResult = await classRoomModelDao.updateByFilter(trans, { name: updatedName }, { name: simpleClass.name }, options);

            assert.ok(updateResult)
            assert.ok(updateResult.data)
            assert.ok(updateResult.data.lastUpdateBy)
            assert.ok(updateResult.data.lastUpdateOn)
            assert.strictEqual(updateResult.data.name, updatedName)


            let findResult = await classRoomModelDao.findByFilter(trans, { name: updatedName}, options);

            assert.strictEqual(findResult.length, 3)


        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });


    it('| updateByFilter --> should throw an error as association id is missing', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModel = new TeacherModel();
        teacherModel.name = chance.string({ length: 10 });


        let options = { user: "__super_user__" };
        let updatedClassName = chance.string({ length: 10 });

        try {

            // 2. execute test
            await teacherModelDao.create(trans, teacherModel, options);
            await classRoomModelDao.create(trans, simpleClass, options);


            await classRoomModelDao.updateByFilter(trans, { name: updatedClassName }, { teacher: teacherModel }, options);
            
            throw new Error('BAD FLOW... we should not be here!');


        } catch (e) {
            if ( e instanceof Exception ) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS')
                assert.strictEqual(e.message, '[ClassRoomModel] association [teacher] is missing the [id] field.');                
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }            
        }
    });


    it('| updateByFilter --> should throw an error as provided association does not exist in database', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModel = new TeacherModel();
        teacherModel.name = chance.string({ length: 10 });
        teacherModel.id   = 15;


        let options = { user: "__super_user__", include: ['teacher'] };
        let updatedClassName = chance.string({ length: 10 });

        try {

            // 2. execute test
            await teacherModelDao.create(trans, teacherModel, options);
            await classRoomModelDao.create(trans, simpleClass, options);


            await classRoomModelDao.updateByFilter(trans, { name: updatedClassName }, { teacher: teacherModel }, options);

            throw new Error('BAD FLOW... we should not be here!');


        } catch (e) {
            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_EXISTS')
                assert.strictEqual(e.message, 'no [TeacherModel] associations with ids [15] exist!');
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| updateByFilter --> should update the class teacher', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModel = new TeacherModel();
        teacherModel.name = chance.string({ length: 10 });
        teacherModel.id = 15;


        let options = { user: "__super_user__", include: ['teacher'] };
        let updatedTeacherName = chance.string({ length: 10 });

        try {

            // 2. execute test
            teacherModel = await teacherModelDao.create(trans, teacherModel, options);
            
            await classRoomModelDao.create(trans, simpleClass, options);

            let updatedTeacherModel = { ...teacherModel, name: updatedTeacherName};
            let updateResult = await classRoomModelDao.updateByFilter(trans, { teacher: updatedTeacherModel }, { teacher: teacherModel }, options);

            assert.ok(updateResult);
            assert.ok(updateResult.data);
            assert.ok(updateResult.data.teacher.name);
            assert.strictEqual(updateResult.data.teacher.name, updatedTeacherName);


        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }
    });


    it('| updateByFilter --> should fail to update model association field as it is not provided in options', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacherModel = new TeacherModel();
        teacherModel.name = chance.string({ length: 10 });
        teacherModel.id = 15;


        let options = { user: "__super_user__", include: [] };
        let updatedTeacherName = chance.string({ length: 10 });

        try {

            // 2. execute test
            teacherModel = await teacherModelDao.create(trans, teacherModel, options);

            await classRoomModelDao.create(trans, simpleClass, options);

            let updatedTeacherModel = { ...teacherModel, name: updatedTeacherName };
            await classRoomModelDao.updateByFilter(trans, { teacher: updatedTeacherModel }, { teacher: teacherModel }, options);

            throw new Error('BAD FLOW... we should not be here!');


        } catch (e) {
            if ( e instanceof Exception) {
                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_INC_OPTS_EXISTS");
                assert.strictEqual(e.message, `[teacher] has to be added in the "options.include" in order to use it!`);

            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }            
        }
    });


    it('| updateByFilter --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let updatedString = chance.string({ length: 10 });

        try {
            // 2. execute test
            await classRoomModelDao.updateByFilter(trans, {name: updatedString}, {name: simpleClass.name});

        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| updateById --> should update one instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let updatedString = chance.string({ length: 10 });

        let options = { user : "__super_user__"}

        try {
            // 2. execute test

            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let updateResult = await classRoomModelDao.updateById(trans, { name: updatedString }, simpleClass.id, options);

            assert.ok(updateResult)
            assert.ok(updateResult.data)            
            assert.ok(updateResult.data.lastUpdateOn)
            assert.ok(updateResult.data.lastUpdateBy)

            let findResult = await classRoomModelDao.findById(trans, simpleClass.id, options);

            assert.strictEqual(findResult.name, updatedString);


        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });

    it('| updateById --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let updatedString = chance.string({ length: 10 });

        try {
            // 2. execute test
            await classRoomModelDao.updateById(trans, { name: updatedString }, 5);

        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| deleteByFilter --> should delete one instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });        

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let updateResult = await classRoomModelDao.deleteByFilter(trans, { name: simpleClass.name }, options);

            assert.ok(updateResult)
            assert.ok(updateResult.meta.affectedCount)
            assert.strictEqual(updateResult.meta.affectedCount, 1)
            

            let findResult = await classRoomModelDao.findByFilter(trans, {name: simpleClass.name}, options);

            assert.strictEqual(findResult.length, 0);


        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });


    it('| deleteByFilter --> should throw an error as the id is not provided in the association', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });        

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass, simpleClass], options);

            let updateResult = await classRoomModelDao.deleteByFilter(trans, { teacher }, options);

            assert.ok(updateResult)
            assert.ok(updateResult.meta.affectedCount)
            assert.strictEqual(updateResult.meta.affectedCount, 3)


            let findResult = await classRoomModelDao.findByFilter(trans, { name: simpleClass.name }, options);

            assert.strictEqual(findResult.length, 0);


        } catch (e) {

            if ( e instanceof Exception) {
                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS")
                assert.strictEqual(e.message, `[ClassRoomModel] association [teacher] is missing the [id] field.`)                
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }                        
        }

    });


    it('| deleteByFilter --> should throw an error as no instance exists with provided id for the association', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });        

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });
        teacher.id = 555;

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass, simpleClass], options);

            let updateResult = await classRoomModelDao.deleteByFilter(trans, { teacher }, {...options, include: ['teacher']});

            assert.ok(updateResult)
            assert.ok(updateResult.meta.affectedCount)
            assert.strictEqual(updateResult.meta.affectedCount, 3)


            let findResult = await classRoomModelDao.findByFilter(trans, { name: simpleClass.name }, options);

            assert.strictEqual(findResult.length, 0);


        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_EXISTS")
                assert.strictEqual(e.message, `no [TeacherModel] associations with ids [${teacher.id}] exist!`)
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| deleteByFilter --> should throw an error as association is not provided in options', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });
        teacher.id = 555;

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass, simpleClass], options);

            let updateResult = await classRoomModelDao.deleteByFilter(trans, { teacher }, options);

            assert.ok(updateResult)
            assert.ok(updateResult.meta.affectedCount)
            assert.strictEqual(updateResult.meta.affectedCount, 3)


            let findResult = await classRoomModelDao.findByFilter(trans, { name: simpleClass.name }, options);

            assert.strictEqual(findResult.length, 0);


        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_INC_OPTS_EXISTS")
                assert.strictEqual(e.message, `[teacher] has to be added in the "options.include" in order to use it!`)
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| deleteByFilter --> should delete instance based on association filter, the association shouldn\'t be deleted', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });        

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            teacher = await teacherModelDao.create(trans, teacher, options);
            simpleClass.teacher = teacher

            await classRoomModelDao.createAll(trans, [simpleClass, simpleClass, simpleClass], {...options, include: ['teacher']});
            
            let updateResult = await classRoomModelDao.deleteByFilter(trans, { teacher }, {...options, include: ['teacher']});

            assert.ok(updateResult)
            assert.ok(updateResult.meta.affectedCount)
            assert.strictEqual(updateResult.meta.affectedCount, 3)

            let findClassResult = await classRoomModelDao.findByFilter(trans, { name: simpleClass.name }, options);            
            assert.strictEqual(findClassResult.length, 0);

            let findTeacherResult = await teacherModelDao.findByFilter(trans, { name: teacher.name }, options);
            assert.strictEqual(findTeacherResult.length, 1);
            assert.strictEqual(findTeacherResult[0].id, teacher.id);
            assert.strictEqual(findTeacherResult[0].name, teacher.name);


        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });


    it('| deleteByFilter --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let updatedString = chance.string({ length: 10 });

        try {
            // 2. execute test
            await classRoomModelDao.deleteByFilter(trans, { name: updatedString });

        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| deleteById --> should delete one instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });


        let options = { user : "__super_user__"}


        try {
            // 2. execute test
            
            simpleClass = await classRoomModelDao.create(trans, simpleClass, options)
            
            let deleteResult = await classRoomModelDao.deleteById(trans, simpleClass.id , options);

            assert.ok(deleteResult)
            assert.ok(deleteResult.meta)
            assert.ok(deleteResult.meta.affectedCount)
            assert.strictEqual(deleteResult.meta.affectedCount, 1);

            let findResult = await classRoomModelDao.findByFilter(trans, {id: simpleClass.id}, options);

            assert.ok(findResult)
            assert.strictEqual(findResult.length, 0);

        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });

    it('| deleteById --> should delete one instance and not delete the association', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" }

        try {
            // 2. execute test

            teacher = await teacherModelDao.create(trans, teacher, options);

            simpleClass.teacher = teacher
            simpleClass = await classRoomModelDao.create(trans, simpleClass, {...options, include: ['teacher']});

            let deleteResult = await classRoomModelDao.deleteById(trans, simpleClass.id, options);

            assert.ok(deleteResult)
            assert.ok(deleteResult.meta)
            assert.ok(deleteResult.meta.affectedCount)
            assert.strictEqual(deleteResult.meta.affectedCount, 1);
            let findTeacherResult = await teacherModelDao.findByFilter(trans, { id: teacher.id }, options);

            assert.ok(findTeacherResult)
            assert.strictEqual(findTeacherResult.length, 1);

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });


    it('| deleteById --> should throw an error as no instance found with the given id', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });
        simpleClass.id = 1511;
                
        let options = { user: "__super_user__" }

        try {
            // 2. execute test
                        
            await classRoomModelDao.deleteById(trans, simpleClass.id, options);

            throw new Error('BAD FLOW... we should not be here!');

        } catch (e) {

            if ( e instanceof Exception ) {
                assert.strictEqual(e.reason, "ERR_TYP_ORM_DAO__DLT_BY_ID__INSTANCE_NOT_FOUND")
                assert.strictEqual(e.message, `no instance of [ClassRoomModel] with id ${simpleClass.id} exists!`)
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }            
        }

    });


    it('| deleteById --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });
        
        try {
            // 2. execute test
            await classRoomModelDao.deleteById(trans, 55);

        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| findByFilter --> should find one instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user : "__super_user__"}

        try {
            // 2. execute test
            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let findResult = await classRoomModelDao.findByFilter(trans, {}, options);

            assert.ok(findResult)
            assert.ok(findResult[0])
            assert.ok(findResult[0].createdOn)
            assert.ok(findResult[0].createdBy)
            assert.strictEqual(findResult[0].id, simpleClass.id)
            assert.strictEqual(findResult[0].name, simpleClass.name)            

        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });

    it('| findByFilter --> should find 10 instances', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" }


        let classes = Array(10).fill(0).map(x => simpleClass);

        try {
            // 2. execute test
            await classRoomModelDao.createAll(trans, classes, options);

            let findResult = await classRoomModelDao.findByFilter(trans, {}, options);

            assert.ok(findResult)            
            assert.strictEqual(findResult.length, 10)
            
            findResult.forEach( _class => {
                assert.strictEqual(_class.name, simpleClass.name)
            })

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| findByFilter --> should find 10 instances with association', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10});

        let options = { user: "__super_user__" }

        try {
            // 2. execute test
            teacher = await teacherModelDao.create(trans, teacher, options);

            simpleClass.teacher = teacher
            let classes = Array(10).fill(0).map(x => simpleClass);
            
            await classRoomModelDao.createAll(trans, classes, {...options, include: ['teacher']});

            let findResult = await classRoomModelDao.findByFilter(trans, {}, {...options, include: ['teacher']});

            assert.ok(findResult)
            assert.strictEqual(findResult.length, 10)

            findResult.forEach(_class => {
                assert.strictEqual(_class.name, simpleClass.name)
                assert.strictEqual(_class.teacher.name, teacher.name)
            })

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| findByFilter --> should find 5 instances with association filter is applied', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let teacher = new TeacherModel();
        teacher.name = chance.string({ length: 10 });

        let teacherFilter = new TeacherModel();
        teacherFilter.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" }

        try {
            // 2. execute test
            teacher = await teacherModelDao.create(trans, teacher, options);
            teacherFilter = await teacherModelDao.create(trans, teacherFilter, options);
            
            let classes = Array(5).fill(0).map(x => { return {...simpleClass, teacher: teacherFilter} });
            let classes2 = Array(5).fill(0).map(x => simpleClass);

            await classRoomModelDao.createAll(trans, classes, { ...options, include: ['teacher'] });
            await classRoomModelDao.createAll(trans, classes2, { ...options, include: ['teacher'] });

            let findResult = await classRoomModelDao.findByFilter(trans, {teacher: teacherFilter}, { ...options, include: ['teacher'] });

            assert.ok(findResult)
            assert.strictEqual(findResult.length, 5)

            findResult.forEach(_class => {
                assert.strictEqual(_class.name, simpleClass.name)
                assert.strictEqual(_class.teacher.name, teacherFilter.name)
            })

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| findByFilter --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data
        
        try {
            // 2. execute test
            await classRoomModelDao.findByFilter(trans, {});
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });



    it('| findAll --> should find all 10 instances', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });
        

        let classes = Array(10).fill(0).map(_ => simpleClass);

        let options = { user: "__super_user__"};

        try {
            // 2. execute test

            await classRoomModelDao.createAll(trans, classes, options);

            let findResult = await classRoomModelDao.findAll(trans, options)

            assert.ok(findResult)
            assert.strictEqual(findResult.length, 10)
            

            findResult.forEach(_class => {
                assert.strictEqual(_class.name, simpleClass.name)                
            })

        } catch (e) {            
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');        
        }

    });


    it('| findAll --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findAll(trans);
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });

    it('| findById --> should find 1 instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });        

        let options = { user: "__super_user__" };

        try {
            // 2. execute test

            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let findResult = await classRoomModelDao.findById(trans, simpleClass.id, options);

            assert.ok(findResult)
            assert.strictEqual(findResult.id, simpleClass.id);            
            
        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });


    it('| findById --> should throw an error as no instance exists with the provided id', async () => {
        // 1. prepare test data
        
        let options = { user: "__super_user__" };


        let id = 555;

        try {
            // 2. execute test            

            await classRoomModelDao.findById(trans, id, options);

            throw new Error('BAD FLOW... we should not be here!');

            
        } catch (e) {
            if ( e instanceof Exception) {
                assert.strictEqual(e.reason, "ERR_TYP_ORM_DAO__FND_BY_ID__DATA_NOT_FOUND")
                assert.strictEqual(e.message, `no instance of [ClassRoomModel] with id ${id} exists!`)                
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }            
        }

    });


    it('| findById --> empty options, should throw user not provided error', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findById(trans, 555);
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| findSingleByFilter --> should find 1 instance', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {
            // 2. execute test

            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let findResult = await classRoomModelDao.findSingleByFilter(trans, {id: simpleClass.id}, options);
            
            assert.ok(findResult)
            assert.strictEqual(findResult && findResult.id, simpleClass.id);

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });



    it('| findSingleByFilter --> should return null given that no instance exists with filter', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {
            // 2. execute test

            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);

            let findResult = await classRoomModelDao.findSingleByFilter(trans, { name: "dont" }, options);

            assert.ok(findResult === null)
            // assert.strictEqual(findResult && findResult.id, simpleClass.id);

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });


    it('| save --> should create 1 instance given that unique keys is provided', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__", uniqueKeys: { name: simpleClass.name} };

        try {
            // 2. execute test

            let createResult = await classRoomModelDao.save(trans, simpleClass, options);

            // let findResult = await classRoomModelDao.findSingleByFilter(trans, { id: simpleClass.id }, options);

            assert.ok(createResult)
            assert.strictEqual(createResult.name, simpleClass.name);

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| save --> should update 1 instance given that instance exist and instance has id', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {
            // 2. execute test
            simpleClass = await classRoomModelDao.create(trans, simpleClass, options);
            simpleClass.name = "new name";            

            let saveResult = await classRoomModelDao.save(trans, simpleClass, options);

            // let findResult = await classRoomModelDao.findSingleByFilter(trans, { id: simpleClass.id }, options);

            assert.ok(saveResult)
            assert.ok(saveResult.id)
            assert.ok(saveResult.lastUpdateBy)
            assert.ok(saveResult.lastUpdateOn)
            assert.strictEqual(saveResult.name, simpleClass.name);

        } catch (e) {
            console.warn(e);
            throw new Error('BAD FLOW... we should not be here!');
        }

    });

    it('| save --> should throw error as no uniqueKeys are provided, nor do we have an id', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        simpleClass.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        try {
            // 2. execute test                  
            await classRoomModelDao.save(trans, simpleClass, options);
            
            throw new Error('BAD FLOW... we should not be here!');
        } catch (e) {

            // 3. assert result
            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ER_COMMONS_DATA_SEQUELIZE__SEQUELIZE_DAO__SAVE__MISS_UNIQUE_KEYS')
                // assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }

    });


    it('| findAll --> should find results in the asc order given that order is provided in options', async () => {
        // 1. prepare test data

        
        let objcs: ClassRoomModel[] = new Array(100).fill(0).map( x => ({name: chance.string({length: 10})} as ClassRoomModel ));

        let options = { user: "__super_user__" };

        await classRoomModelDao.createAll(trans, objcs, options);

        try {
            // 2. execute test                  
            
            let result = await classRoomModelDao.findAll(trans, {...options, orderBy: {id: "ASC"}} )

            let prev: any = null;

            for ( let elem of result ) {
                
                if ( prev === null) {
                    prev = elem;
                    continue;
                }

                // assert result                
                assert.ok(prev.id < elem.id);                
                prev = elem;
            }

            assert.strictEqual(result.length , 100)
            
        } catch (e) {
            console.error(e)
            throw new Error('BAD FLOW... we should not be here!');            
        }

    });

  

});
