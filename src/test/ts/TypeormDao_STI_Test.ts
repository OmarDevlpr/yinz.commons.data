// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "../../main/ts/DataSource";
import { TypeormDao, TypeormDataSource } from "../../main/";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { Salary, StaffSalary, ManagerSalary, SalaryPayementLog } from "./test-data/STIParentModel";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;

const salaryDao = new TypeormDao<Salary>({ model: Salary, logger: logger, accessManager: accessManager })
const staffSalaryDao = new TypeormDao<StaffSalary>({ model: StaffSalary, logger: logger, accessManager: accessManager })
const managerSalaryDao = new TypeormDao<ManagerSalary>({ model: ManagerSalary, logger: logger, accessManager: accessManager })
const salaryPayementLogDao = new TypeormDao<SalaryPayementLog>({ model: SalaryPayementLog, logger: logger, accessManager: accessManager })


container.setClazz('Salary', Salary);
container.setClazz('StaffSalary', StaffSalary);
container.setClazz('ManagerSalary', ManagerSalary);
container.setClazz('SalaryPayementLog', SalaryPayementLog);

container.setBean('salaryDao', salaryDao);
container.setBean('staffSalaryDao', staffSalaryDao);
container.setBean('managerSalaryDao', managerSalaryDao);
container.setBean('salaryPayementLogDao', salaryPayementLogDao);



describe('| commons.data.TypeormDao_STI_Test', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    it('| should pass', async () => {

        const options = {user: "__super_user__"};

        // 1. prepare test data
        let staffSalary = new StaffSalary();
        staffSalary.amount = chance.integer({min: 100, max: 2000});
        staffSalary = await staffSalaryDao.create(trans, staffSalary, options)

        let managerSalary = new ManagerSalary();
        managerSalary.amount = chance.integer({ min: 100, max: 2000 });
        managerSalary.descr = chance.string({length: 10})
        managerSalary = await managerSalaryDao.create(trans, managerSalary, options)

        let staffSalaryPayementLog = new SalaryPayementLog();
        staffSalaryPayementLog.amount = staffSalary.amount
        staffSalaryPayementLog.salary = staffSalary;
        staffSalaryPayementLog = await salaryPayementLogDao.create(trans, staffSalaryPayementLog, {...options, include: ['salary']});


        let managerSalaryPayementLog = new SalaryPayementLog();
        managerSalaryPayementLog.amount = managerSalary.amount
        managerSalaryPayementLog.descr = managerSalary.descr
        managerSalaryPayementLog.salary = managerSalary;
        managerSalaryPayementLog = await salaryPayementLogDao.create(trans, managerSalaryPayementLog, { ...options, include: ['salary'] });
                
        assert.ok(true)
    });


});