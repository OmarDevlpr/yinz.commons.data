// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger, YinzOptions, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "../../main/ts/DataSource";
import { TypeormDao, TypeormDataSource } from "../../main/";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { SampleModel } from "./test-data/SampleModel";
import { TeacherModel } from "./test-data/TeacherModel";
import { ClassRoomModel } from "./test-data/ClassRoomModel";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


const sampleModelDao = new TypeormDao<SampleModel>({
    model: SampleModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('SampleModel', SampleModel);
container.setBean('sampleModelDao', sampleModelDao);

const teacherModelDao = new TypeormDao<TeacherModel>({
    model: TeacherModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('TeacherModel', TeacherModel);
container.setBean('teacherModelDao', teacherModelDao);

const classRoomModelDao = new TypeormDao<ClassRoomModel>({
    model: ClassRoomModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('ClassRoomModel', ClassRoomModel);
container.setBean('classRoomModelDao', classRoomModelDao);

const VALID_USER = "valid_user";
const INVALID_USER = "invalid_user";


describe('| commons.data.TypeormDao <AccessControl> ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)            
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });


    it('| (all dao crud functions) --> should execute successfully with super user', async () => {
        // 1. prepare test data

        let sampleModelDto = new SampleModel();
        sampleModelDto.name = chance.string({ length: 10 });

        let options = { user: "__super_user__" };

        // 2. execute test

        let sampleModelDto1 = await sampleModelDao.create(trans, sampleModelDto, options);
        await sampleModelDao.createAll(trans, [sampleModelDto], options);
        await sampleModelDao.updateByFilter(trans, { name: sampleModelDto.name }, {name: sampleModelDto.name}, options);
        await sampleModelDao.updateById(trans, { name: sampleModelDto.name }, sampleModelDto1.id, options);
        await sampleModelDao.findAll(trans, options);
        await sampleModelDao.findById(trans, sampleModelDto1.id, options);
        await sampleModelDao.findByFilter(trans, {name: sampleModelDto.name}, options);
        await sampleModelDao.deleteByFilter(trans, {name: "dont"}, options);
        await sampleModelDao.deleteById(trans, sampleModelDto1.id, options);
        
    });


    it('| (all dao crud functions) --> should execute successfully with valid user', async () => {
        // 1. prepare test data

        let sampleModelDto = new SampleModel();
        sampleModelDto.name = chance.string({ length: 10 });

        let options = { user: VALID_USER };

        // 2. execute test

        let sampleModelDto1 = await sampleModelDao.create(trans, sampleModelDto, options);
        await sampleModelDao.createAll(trans, [sampleModelDto], options);
        await sampleModelDao.updateByFilter(trans, { name: sampleModelDto.name }, { name: sampleModelDto.name }, options);
        await sampleModelDao.updateById(trans, { name: sampleModelDto.name }, sampleModelDto1.id, options);
        await sampleModelDao.findAll(trans, options);
        await sampleModelDao.findById(trans, sampleModelDto1.id, options);
        await sampleModelDao.findByFilter(trans, { name: sampleModelDto.name }, options);
        await sampleModelDao.deleteByFilter(trans, { name: "dont" }, options);
        await sampleModelDao.deleteById(trans, sampleModelDto1.id, options);
    });


    it('| (all dao crud functions) --> should execute successfully with actionGranted in options and invalid user', async () => {
        // 1. prepare test data

        let sampleModelDto = new SampleModel();
        sampleModelDto.name = chance.string({ length: 10 });

        let options: YinzOptions = { __$$actionIsGranted$$__: true, user: INVALID_USER };

        // 2. execute test

        let sampleModelDto1 = await sampleModelDao.create(trans, sampleModelDto, options);
        await sampleModelDao.createAll(trans, [sampleModelDto], options);
        await sampleModelDao.updateByFilter(trans, { name: sampleModelDto.name }, { name: sampleModelDto.name }, options);
        await sampleModelDao.updateById(trans, { name: sampleModelDto.name }, sampleModelDto1.id, options);
        await sampleModelDao.findAll(trans, options);
        await sampleModelDao.findById(trans, sampleModelDto1.id, options);
        await sampleModelDao.findByFilter(trans, { name: sampleModelDto.name }, options);
        await sampleModelDao.deleteByFilter(trans, { name: "dont" }, options);
        await sampleModelDao.deleteById(trans, sampleModelDto1.id, options);
    });
    

    it('| create --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();
        
        try {
            // 2. execute test
            await classRoomModelDao.create(trans, simpleClass, { __$$actionIsGranted$$__: true});
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| createAll --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();

        try {
            // 2. execute test
            await classRoomModelDao.createAll(trans, [simpleClass], { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| updateByFilter --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.updateByFilter(trans, { name: "foo" }, {name: "foo"}, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| updateById --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data
        
        try {
            // 2. execute test
            await classRoomModelDao.updateById(trans, {name: "foo"}, 55, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| findAll --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findAll(trans, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });



    it('| findById --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findById(trans, 5, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| findByFilter --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findByFilter(trans, {id: {op: "IN", val: [1, 2, 3] }}, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| deleteByFilter --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.deleteByFilter(trans, {name: "foo"}, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });



    it('| deleteById --> should throw an error if user is not present, even if action is granted', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.deleteById(trans, 5, { __$$actionIsGranted$$__: true });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'The user is missing! You should supply who is executing the request in options.')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    
    it('| create --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();

        try {
            // 2. execute test
            await classRoomModelDao.create(trans, simpleClass, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [create] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| createAll --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        let simpleClass = new ClassRoomModel();

        try {
            // 2. execute test
            await classRoomModelDao.createAll(trans, [simpleClass], { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [create] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| updateByFilter --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.updateByFilter(trans, { name: "foo" }, { name: "foo" }, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [update] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| updateById --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.updateById(trans, { name: "foo" }, 55, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [update] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });

    it('| findAll --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findAll(trans, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [read] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });



    it('| findById --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findById(trans, 5, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [read] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| findByFilter --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.findByFilter(trans, { id: { op: "between", val: [5, 6]} }, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [read] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


    it('| deleteByFilter --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.deleteByFilter(trans, { name: "foo" }, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [delete] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });



    it('| deleteById --> should throw an error if user is invalid', async () => {
        // 1. prepare test data

        try {
            // 2. execute test
            await classRoomModelDao.deleteById(trans, 5, { user: INVALID_USER });
        } catch (e) {

            if (e instanceof Exception) {
                assert.strictEqual(e.reason, 'ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED')
                assert.strictEqual(e.message, 'the user [invalid_user] is not granted [delete] on [ClassRoomModel]')
            } else {
                console.warn(e);
                throw new Error('BAD FLOW... we should not be here!');
            }
        }
    });


});
