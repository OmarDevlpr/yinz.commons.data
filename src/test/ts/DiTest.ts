import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',   
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| commons.data.Di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('Model'));
            assert.ok(container.getClazz('TypeormDao'));
            assert.ok(container.getClazz('TypeormDataSource'));            


            // beans
            assert.ok(container.getBean('typeormDataSource'));


        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
