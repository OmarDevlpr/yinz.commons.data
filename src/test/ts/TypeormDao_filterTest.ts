// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger, /*YinzOptions, Exception*/ } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "../../main/ts/DataSource";
import { TypeormDao, TypeormDataSource } from "../../main/";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { SampleModel } from "./test-data/SampleModel";
import { TeacherModel } from "./test-data/TeacherModel";
import { ClassRoomModel } from "./test-data/ClassRoomModel";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


const sampleModelDao = new TypeormDao<SampleModel>({
    model: SampleModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('SampleModel', SampleModel);
container.setBean('sampleModelDao', sampleModelDao);

const teacherModelDao = new TypeormDao<TeacherModel>({
    model: TeacherModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('TeacherModel', TeacherModel);
container.setBean('teacherModelDao', teacherModelDao);

const classRoomModelDao = new TypeormDao<ClassRoomModel>({
    model: ClassRoomModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('ClassRoomModel', ClassRoomModel);
container.setBean('classRoomModelDao', classRoomModelDao);


describe('| commons.data.TypeormDao <Filters> ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)            
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });


    it('| # Equal operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for ( let i = 0; i < 10; i++ ) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({length: 10});
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, {rank: {op: "=", val: 1} } , options)
        
        assert.ok(findResult)
        assert.ok(findResult[0])        

        assert.strictEqual(findResult.length, 1)
        assert.strictEqual(findResult[0].rank, 1)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "=", val: 1 } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 1);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, {name: {op: "=", val: updatedName}}, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 1);

        
    });
    

    // it('| # Not Equal operator (find,update,delete) --> should return desired values', async () => {
    //     // 1. prepare test data

    //     let sampleModels: SampleModel[] = [];


    //     for (let i = 0; i < 10; i++) {
    //         let sampleModel = new SampleModel();
    //         sampleModel.name = chance.string({ length: 10 });
    //         sampleModel.rank = i;
    //         sampleModels.push(sampleModel);
    //     }

    //     let options = { user: "__super_user__" };
    //     let updatedName = chance.string({ length: 10 });

    //     sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


    //     // 2. execute test

    //     let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "!=", val: 1 } }, options)

    //     assert.ok(findResult)
    //     assert.ok(findResult[0])

    //     assert.strictEqual(findResult.length, 9)        

    //     let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "!=", val: 1 } }, options)
    //     let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "!=", val: updatedName } }, options)

    //     assert.ok(updateResult)
    //     assert.ok(findResult2)

    //     assert.strictEqual(findResult2.length, 1); // 9 rows have the updated name, which means one instance doesn't have updatedName

    //     let deleteResult = await sampleModelDao.deleteByFilter(trans, { name: { op: "!=", val: updatedName } }, options);
    //     let findResult3 = await sampleModelDao.findByFilter(trans, { name: { op: "!=", val: updatedName } }, options)

    //     assert.ok(deleteResult)
    //     assert.ok(findResult3)

    //     assert.strictEqual(findResult3.length, 0);
    //     assert.strictEqual(deleteResult.meta.affectedCount, 1);


    // });


    it('| # Less Than operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "<", val: 5 } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 5)        

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "<", val: 5 } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 5);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "<", val: 5 } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "<", val: 5 } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 5);


    });

    it('| # Less Than Or Equal operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "<=", val: 5 } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 6)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "<=", val: 5 } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 6);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "<=", val: 5 } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "<=", val: 5 } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 6);


    });


    it('| # Greater Than operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: ">", val: 5 } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 4)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: ">", val: 5 } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 4);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: ">", val: 5 } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: ">", val: 5 } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 4);


    });


    it('| # Greater Than or Equal operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: ">=", val: 5 } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 5)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: ">=", val: 5 } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 5);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: ">=", val: 5 } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: ">=", val: 5 } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 5);


    });



    it('| # in operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "in", val: [0, 1, 2] } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 3)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "in", val: [0, 1, 2] } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 3);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "in", val: [0, 1, 2] } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "in", val: [0, 1, 2] } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 3);


    });


    it('| # IN operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "IN", val: [0, 1, 2] } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 3)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "IN", val: [0, 1, 2] } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 3);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "IN", val: [0, 1, 2] } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "IN", val: [0, 1, 2] } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 3);

    });


    it('| # between operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "between", val: [1, 5] } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 5)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "between", val: [1, 5] } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 5);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "between", val: [1, 5] } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "between", val: [1, 5] } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 5);

    });



    it('| # BETWEEN operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];


        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updatedName = chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { rank: { op: "BETWEEN", val: [1, 5] } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 5)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { rank: { op: "BETWEEN", val: [1, 5] } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "=", val: updatedName } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 5);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { rank: { op: "BETWEEN", val: [1, 5] } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { rank: { op: "BETWEEN", val: [1, 5] } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 5);

    });

    it('| # like operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];

        let namePrefix = chance.string({ length: 10 });

        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = namePrefix + chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updateNamePrefix = chance.string({ length: 10 });
        let updatedName = updateNamePrefix + chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { name: { op: "like", val: `%${namePrefix}%` } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 10)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { name: { op: "like", val: `%${namePrefix}%` } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "like", val: `%${updatedName}%` } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 10);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { name: { op: "like", val: `%${updatedName}%` } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { name: { op: "like", val: `%${updatedName}%` } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 10);

    });


    it('| # LIKE operator (find,update,delete) --> should return desired values', async () => {
        // 1. prepare test data

        let sampleModels: SampleModel[] = [];

        let namePrefix = chance.string({ length: 10 });

        for (let i = 0; i < 10; i++) {
            let sampleModel = new SampleModel();
            sampleModel.name = namePrefix + chance.string({ length: 10 });
            sampleModel.rank = i;
            sampleModels.push(sampleModel);
        }

        let options = { user: "__super_user__" };
        let updateNamePrefix = chance.string({ length: 10 });
        let updatedName = updateNamePrefix + chance.string({ length: 10 });

        sampleModels = await sampleModelDao.createAll(trans, sampleModels, options);


        // 2. execute test

        let findResult = await sampleModelDao.findByFilter(trans, { name: { op: "LIKE", val: `%${namePrefix}%` } }, options)

        assert.ok(findResult)
        assert.ok(findResult[0])

        assert.strictEqual(findResult.length, 10)

        let updateResult = await sampleModelDao.updateByFilter(trans, { name: updatedName }, { name: { op: "LIKE", val: `%${namePrefix}%` } }, options)
        let findResult2 = await sampleModelDao.findByFilter(trans, { name: { op: "LIKE", val: `%${updatedName}%` } }, options)

        assert.ok(updateResult)
        assert.ok(findResult2)

        assert.strictEqual(findResult2.length, 10);

        let deleteResult = await sampleModelDao.deleteByFilter(trans, { name: { op: "LIKE", val: `%${updatedName}%` } }, options);
        let findResult3 = await sampleModelDao.findByFilter(trans, { name: { op: "LIKE", val: `%${updatedName}%` } }, options)

        assert.ok(deleteResult)
        assert.ok(findResult3)

        assert.strictEqual(findResult3.length, 0);
        assert.strictEqual(deleteResult.meta.affectedCount, 10);

    });

});
