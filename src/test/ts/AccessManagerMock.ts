export default class AccessManagerMock {

        constructor() {

        }

        async isUserGrantedAuthzes(handler, user, authzes) {                

                if ( authzes.action === "create") {
                        if ( user === "valid_user") {
                                return await {};
                        }       
                        return await null;                 
                }

                if (authzes.action === "read") {
                        if (user === "valid_user") {
                                return await {};
                        }
                        return await null;
                }

                if (authzes.action === "update") {
                        if (user === "valid_user") {
                                return await {};
                        }
                        return await null;
                }

                if (authzes.action === "delete") {
                        if (user === "valid_user") {
                                return await {};
                        }
                        return await null;
                }

                return await null;

        }

}