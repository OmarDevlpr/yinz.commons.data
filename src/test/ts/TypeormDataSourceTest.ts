// libs
import * as assert from "assert";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger /*, Exception*/ } from "@yinz/commons";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { SampleModel } from "./test-data/SampleModel";
import { TypeormDao } from "../../main/";
import TypeormDataSource from "../../main/ts/TypeormDataSource";




const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/access.data/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});


const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');

const sampleModelDao = new TypeormDao<SampleModel>({
    model: SampleModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('SampleModel', SampleModel);
container.setBean('sampleModelDao', sampleModelDao);




describe('| commons.data.typeormDataSource', function () {

    it('| should create a new connection, start a transaction and commit it without errors', async () => {
        // 1. prepare test data        
        let dataSource = new TypeormDataSource(container.getClazzes(), container.getParam('db'));
        
        let conn = await dataSource.getConn();

        // commit trans
        let trans = await dataSource.startTrans(conn);        
        await dataSource.commitTrans(trans);
        trans = await dataSource.startTrans(conn);


        // let sampleModelRepo = conn.native.getRepository(SampleModel);

        // // let modelKeys = sampleModelRepo.metadata.columns.map(_ => _.propertyName);
        // let modelName =  sampleModelRepo.metadata.tableName;

        // const sqlProcedure = `
        // CREATE OR REPLACE FUNCTION notify_trigger()
        //     RETURNS trigger AS
        // $func$
        // DECLARE
        //     payload TEXT;
        // BEGIN
        //     payload := '{ "table":"' || TG_TABLE_NAME || '"}';
        //     PERFORM pg_notify('db_notifications', payload);
        //     RETURN NULL;
        // END
        // $func$  LANGUAGE plpgsql;`
        // const sqlProcedureRsp = await trans.conn.native.connection.manager.query(sqlProcedure)
        // console.log(sqlProcedureRsp);

        // const sql =
        //     `
        //         CREATE TRIGGER ${modelName}_notify AFTER INSERT OR UPDATE OR DELETE ON ${modelName}
        //         FOR EACH STATEMENT EXECUTE PROCEDURE notify_trigger();                            
        //     `

        // const sqlRsp = await trans.conn.native.connection.manager.query(sql)
        // console.log(sqlRsp);

        // // const sql2 = 'select tgname from pg_trigger;';
        // // const sqlRsp2 = await conn.native.connection.manager.query(sql2)
        // // console.log(sqlRsp2);



        await dataSource.rollbackTrans(trans);
    
        assert.ok(conn)
        assert.ok(trans)        
        
        await dataSource.relConn(conn);

    });


});
