// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "../../main/ts/DataSource";
import { TypeormDao, TypeormDataSource } from "../../main/";
// test dependencies
import AccessManagerMock from "./AccessManagerMock";
import { SampleModel } from "./test-data/SampleModel";
import { I18nSampleModel } from "./test-data/I18nSampleModel";
import { Salary } from "./test-data/STIParentModel";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        // '@yinz/cache.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const accessManager = new AccessManagerMock();
const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


const sampleModelDao = new TypeormDao<SampleModel>({
    model: SampleModel,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('SampleModel', SampleModel);
container.setBean('sampleModelDao', sampleModelDao);

const salaryDao = new TypeormDao<Salary>({
    model: Salary,
    logger: logger,
    accessManager: accessManager
});
container.setClazz('Salary', Salary);
container.setBean('Salary', salaryDao);

const i18nSampleModelDao = new TypeormDao<I18nSampleModel>({
    model: I18nSampleModel,
    logger: logger,
    accessManager: accessManager
})

container.setClazz('I18nSampleModel', I18nSampleModel);
container.setBean('i18nSampleModelDao', i18nSampleModelDao);



describe('| commons.data.TypeormDaoI18n', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    it('| should return fields with local provided in options', async () => {

        // 1. prepare test data

        let sampleModelDto = new SampleModel();
        sampleModelDto.rank = chance.integer({min: 10, max: 500});

        let i18nSampleModelDtoAr = new I18nSampleModel();
        i18nSampleModelDtoAr.locale = 'ar';
        i18nSampleModelDtoAr.name = 'ثامر';

        let i18nSampleModelDtoFr = new I18nSampleModel();
        i18nSampleModelDtoFr.locale = 'fr';
        i18nSampleModelDtoFr.name = 'Thamer';

        sampleModelDto = await sampleModelDao.create(trans, sampleModelDto, {user: "__super_user__"});
        await i18nSampleModelDao.create(trans, {...i18nSampleModelDtoAr, ownerId: sampleModelDto.id}, {user: "__super_user__"});
        await i18nSampleModelDao.create(trans, {...i18nSampleModelDtoFr, ownerId: sampleModelDto.id}, {user: "__super_user__"});        
                
        let options = { user: "__super_user__" };

        // 2. execute test
        let resultAr = await sampleModelDao.findByFilter(trans, {rank: sampleModelDto.rank}, {...options, locale: 'ar'});
        let resultFr = await sampleModelDao.findByFilter(trans, {rank: sampleModelDto.rank}, {...options, locale: 'fr'});

        // 3. compare result
        assert.ok(resultAr);
        assert.strictEqual(resultAr.length, 1);

        assert.ok(resultFr);
        assert.strictEqual(resultFr.length, 1);

        assert.ok(resultAr[0])
        assert.ok(resultFr[0])

        assert.strictEqual(resultAr[0].rank, sampleModelDto.rank);
        assert.strictEqual(resultFr[0].rank, sampleModelDto.rank);
        
        assert.strictEqual(resultAr[0].name, i18nSampleModelDtoAr.name);
        assert.strictEqual(resultFr[0].name, i18nSampleModelDtoFr.name);
    });


    it('| should skip local as the model does not have an I18n model', async () => {

        // 1. prepare test data

        let salary = new Salary();
        salary.amount = chance.integer({min: 540, max: 4000});
        salary.ref = 'phantom';
        salary = await salaryDao.create(trans, salary, {user: "__super_user__"});

        // 2. execute test
        let result = await salaryDao.findAll(trans, {user: "__super_user__", locale: 'ar'});

        // 3. check outcome
        assert.ok(result)
        assert.strictEqual(result.length, 1)
        assert.strictEqual(result[0].ref, salary.ref)
        
    });

});