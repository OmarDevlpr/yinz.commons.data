import Dao, { DeleteResult, UpdateResult } from "./Dao";
import { YinzConnection, YinzTransaction } from "./DataSource";
import { YinzOptions } from "@yinz/commons/ts/YinzOptions";
import Logger from "@yinz/commons/ts/Logger";
import { Repository, In, Between, Like, LessThan, MoreThan, Equal, DeepPartial } from "typeorm";
import Exception from "@yinz/commons/ts/Exception";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import { ModelFilterOp, WhereFilter, ModelOrderOp } from "./FilterTypes";
import ICacheManager from "@yinz/cache.services/ts/ICacheManager";

export interface TypeormDaoOptions {
    model: any;
    logger: Logger;
    accessManager: any;
}

type actionType = "create" | "delete" | "update" | "read" | "execute" | "any";


export interface ModelDefaultFields {
    lastUpdateBy: string;
    lastUpdateOn: Date;
    createdBy: string;
    createdOn: Date;
    id: number;
}

export type FilterType<A> = DeepPartial<A> | ModelFilterOp<DeepPartial<A>> | DeepPartial<A> & ModelFilterOp<DeepPartial<A>>;

export default class TypeormDao<A extends ModelDefaultFields> implements Dao<A> {


    private _model;
    private _logger: Logger;
    private _accessManager: AccessManager;
    private _modelName: string;

    public enableCaching: boolean;
    public cacheManager: ICacheManager;

    constructor(options: TypeormDaoOptions) {
        this._modelName = options.model.name;
        this._model = options.model;
        this._logger = options.logger;
        this._accessManager = options.accessManager;

    }

    private async assertAssocExists(repo: Repository<any>, assoc: any, options?: YinzOptions) {

        assoc = Array.isArray(assoc) ? assoc : [assoc];
        let ids = assoc.map(a => a.id);
        let result = await repo.findByIds(ids);

        if (result.length !== assoc.length) {

            let missingIds = assoc.
                filter(a => !result.map(r => r.id).includes(a.id))
                .map(a => a.id)

            throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_EXISTS', {
                message: `no [${repo.metadata.name}] associations with ids [${missingIds}] exist!`,
            })

        }

    }

    private assertAssocIncludeOptionsExist(repo: Repository<A>, object: any, options?: YinzOptions) {

        options = options || {};
        options.include = options.include || [];

        for (let relation of repo.metadata.ownRelations) {

            if (!object[relation.propertyName])
                continue;

            if (!options.include.includes(relation.propertyName)) {
                throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_ASSOC_INC_OPTS_EXISTS', {
                    message: `[${relation.propertyName}] has to be added in the "options.include" in order to use it!`
                })
            }
        }

    }


    private assertValidFields(handler: YinzConnection | YinzTransaction, repo: Repository<A>, object: any, options?: YinzOptions): void {

        options = options || {};

        this._logger.trace('Enter <%s>.assertValidFields(repo: %s, object: %j, options: %j)', this._modelName, repo, object, options);


        let modelFields = Object.keys(repo.metadata.propertiesMap);
        let objectFields = Object.keys(object);

        // 1. Assert model fields 

        for (let field of objectFields) {
            if (!modelFields.includes(field)) {
                throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS', {
                    message: `field [${field}] does not belong to [${this._modelName}], valid fields are ${[...modelFields]}`
                })
            }
        }

        // 2. Assert model assocs fields if any

        let assosiactions = repo.metadata.ownRelations;

        for (let association of assosiactions) {

            if (!object.hasOwnProperty(association.propertyName)) {
                continue;
            }

            let assocRepo = handler.native.getRepository(association.type);

            let assocFields = Object.keys(assocRepo.metadata.propertiesMap);
            let assocObjFields = object[association.propertyName];
            assocObjFields = Array.isArray(assocObjFields) ? assocObjFields : [assocObjFields];


            for (let assocObjField of assocObjFields) {

                let assocFieldKeys = Object.keys(assocObjField);

                for (let field of assocFieldKeys) {
                    if (!assocFields.includes(field)) {
                        throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS', {
                            message: `field [${field}] does not belong to [${association.inverseEntityMetadata.name}], valid fields are ${[...assocFields]}.`
                        })
                    }
                }

                // Make sure that all the assoc objects have an id 
                if (!assocObjField.id) {
                    throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ASSERT_VALID_FIELDS', {
                        message: `[${this._modelName}] association [${association.propertyName}] is missing the [id] field.`
                    })
                }
            }
        }



        this._logger.trace('Leave <%s>.assertValidFields()', this._modelName);

    }

    private async assertActionGranted(handler: YinzConnection | YinzTransaction, action: actionType, options: YinzOptions) {


        this._logger.trace('Enter <%s>.assertActionGranted(handler: %s, action: %j, options: %j)', this._modelName, handler, action, options);

        if (!options.user) {
            throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED', {
                message: `The user is missing! You should supply who is executing the request in options.`,
                action: action,
                options: options
            });
        }

        if (options.user === '__super_user__') {
            return;
        }

        if (options.__$$actionIsGranted$$__) {
            return;
        }

        let result = await this._accessManager.isUserGrantedAuthzes(handler, options.user, {
            subject: this._modelName,
            action: action
        });

        if (result === null) {
            throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED', {
                message: `the user [${options.user}] is not granted [${action}] on [${this._modelName}]`,
                action: action,
                options: options
            });
        }

        options.__$$actionIsGranted$$__ = true;

        this._logger.trace('Leave <%s>.assertActionGranted()', this._modelName);
    }

    private _isLogModel(repo: Repository<A>, options?: YinzOptions): boolean {

        let fields = Object.keys(repo.metadata.propertiesMap);

        if (
            fields.includes("reqRef") &&
            fields.includes("reqDate") &&
            fields.includes("reqSysDate") &&
            fields.includes("reqUser") &&
            fields.includes("reqLang") &&
            fields.includes("tokenCode") &&
            fields.includes("rspRef") &&
            fields.includes("rspDate") &&
            fields.includes("rspSysDate") &&
            fields.includes("rspCode") &&
            fields.includes("rspReason") &&
            fields.includes("rspMessage") &&
            fields.includes("rspExtra")
        ) {
            return true
        }

        return false;
    }

    private translateFilter(filter: FilterType<A>) {

        let typeormFilter: any = {};

        for (let key in filter) {

            let currentFilter = filter[key];

            if (currentFilter && currentFilter.hasOwnProperty("op") && currentFilter.hasOwnProperty("val")) {

                // @ts-ignore typescript is going mad here
                currentFilter = currentFilter as WhereFilter;

                // @ts-ignore typescript is going mad here
                let op = currentFilter.op;
                // @ts-ignore typescript is going mad here
                let val = currentFilter.val;

                switch (op) {
                    case "=": typeormFilter[key] = Equal(val); break;
                    // case "!="      : typeormFilter[key] = Not(Equal(val))                ; break;         
                    case "<": typeormFilter[key] = LessThan(val); break;
                    case "<=": typeormFilter[key] = LessThan((val as number + 1)); break;
                    case ">": typeormFilter[key] = MoreThan(val); break;
                    case ">=": typeormFilter[key] = MoreThan((val as number - 1)); break;
                    case "IN": typeormFilter[key] = In(val as number[]); break;
                    case "in": typeormFilter[key] = In(val as number[]); break;
                    case "BETWEEN": typeormFilter[key] = Between(val[0], val[1]); break;
                    case "between": typeormFilter[key] = Between(val[0], val[1]); break;
                    case "like": typeormFilter[key] = Like(val); break;
                    case "LIKE": typeormFilter[key] = Like(val); break;

                    default: throw new Exception("ERR_COMMONS_DATA__TYP_ORM_DAO__INV_FILTER_OP", {
                        message: `Invalid filter operator [${op}]`,
                        filter: filter,
                        op: op
                    })
                }

            } else {
                typeormFilter[key] = currentFilter;
            }
        }

        return typeormFilter;
    }

    async create(handler: YinzConnection | YinzTransaction, object: DeepPartial<A>, options?: YinzOptions): Promise<A> {

        this._logger.trace('Enter <%s>.create(handler: %s, action: %j, options: %j)', this._modelName, handler, object, options);

        options = options || {};
        let user = options.user;


        // 1. check if access is granted
        await this.assertActionGranted(handler, 'create', options);

        // 2. get repository
        let repo = handler.native.getRepository<A>(this._model);

        // 3. clone the object        

        let clone = Object.assign({}, { createdOn: new Date(), createdBy: user }, object);

        // 4. assert `object` fields
        this.assertValidFields(handler, repo, clone, options);

        // 4.1 if assoc exists with an id, make sure that we have an instance with that id in the database.

        if (repo.metadata.ownRelations.length > 0) {

            for (let rel of repo.metadata.ownRelations) {
                let assocName = rel.propertyName;
                let assocModel = rel.type
                let assocRepo = handler.native.getRepository(assocModel);

                if (clone[assocName])
                    await this.assertAssocExists(assocRepo, clone[assocName], options)

            }
        }

        // 4.2 if assoc is included, make sure it is provided in options
        if (!this._isLogModel(repo, options)) {
            this.assertAssocIncludeOptionsExist(repo, clone, options)
        }


        // 5. create `object`
        let instance: any = await repo.save(clone);

        // 6. return object result

        let result: any = Object.assign({}, clone, {
            id: instance.id,
            createdBy: clone.createdBy,
            createdOn: clone.createdOn
        })

        this._logger.trace('Leave <%s>.create() -> %j', this._modelName, result);

        return result;

    }

    async createAll(handler: YinzConnection | YinzTransaction, objects: DeepPartial<A>[], options?: YinzOptions): Promise<A[]> {

        let createPromises: Promise<A>[] = [];

        for (let object of objects) {
            createPromises.push(this.create(handler, object, options));
        }

        let createResults = await Promise.all(createPromises);

        return createResults;
    }

    async updateByFilter(handler: YinzConnection | YinzTransaction, fields: DeepPartial<A>, filter: FilterType<A>, options?: YinzOptions): Promise<UpdateResult<A>> {
        this._logger.trace('Enter <%s>.update(handler: %s, fields: %j, filter: %j options: %j)', this._modelName, handler, fields, filter, options);

        options = options || {};
        let user = options.user;

        // 1. check if access is granted
        await this.assertActionGranted(handler, 'update', options);

        // 2. get repository
        let repo = handler.native.getRepository<A>(this._model);

        // 3. clone the object        
        let clone = Object.assign({}, fields, { lastUpdateOn: new Date(), lastUpdateBy: user });

        // 4. assert `filter` fields
        this.assertValidFields(handler, repo, filter, options);

        // 5. assert `fields` 
        this.assertValidFields(handler, repo, clone, options);

        // 5.1 if assoc is included, make sure it is provided in options
        if (!this._isLogModel(repo, options)) {
            this.assertAssocIncludeOptionsExist(repo, fields, options)
            this.assertAssocIncludeOptionsExist(repo, filter, options)
        }
        // 5.2 if assoc exists with an id, make sure that we have an instance with that id in the database.

        if (repo.metadata.ownRelations.length > 0) {
            for (let rel of repo.metadata.ownRelations) {
                let assocName = rel.propertyName;
                let assocModel = rel.type
                let assocRepo = handler.native.getRepository(assocModel);

                if (clone[assocName] || filter[assocName])
                    await this.assertAssocExists(assocRepo, clone[assocName] || filter[assocName], options)

            }
        }

        // 5.3 translate filter         
        filter = this.translateFilter(filter)

        // 6. create `object`



        let instance: any = await repo.createQueryBuilder()
            .update()
            .set(clone)
            .where(filter)
            .execute();

        // 7. return object result

        this._logger.trace('Leave <%s>.udpate() -> %j', this._modelName, instance);

        // currently typeorm only returns affected count for mysql! think of a work arround
        return {
            data: clone as DeepPartial<A>,
            meta: {}
        } as any;
    }
    async updateById(handler: YinzConnection | YinzTransaction, fields: DeepPartial<A>, id: number, options?: YinzOptions): Promise<UpdateResult<A>> {

        let result = (await this.updateByFilter(handler, fields, { id: id } as any, options));

        // TODO ENABLE THE CODE BELOW ONCE WE MANAGE TO GET THE METADATA FROM TYPEORM

        // if ( result.meta.affectedCount && result.meta.affectedCount === 0) {
        //     throw new Exception('ERR_TYP_ORM_DAO__UPDT_BY_ID__INSTANCE_NOT_FOUND', {
        //         message: `no instance of [${this._modelName}] with id ${id} exists!`
        //     })
        // }

        // if ( result.meta.affectedCount && result.meta.affectedCount > 1) {
        //     throw new Exception('ERR_TYP_ORM_DAO__UPDT_BY_ID__TOO_MANY_UPDATE', {
        //         message: `too many instances of [${this._modelName}] with id ${id} were updated!`
        //     })
        // }

        return result;
    }

    async deleteByFilter(handler: YinzConnection | YinzTransaction, filter: FilterType<A>, options?: YinzOptions): Promise<DeleteResult> {


        this._logger.trace('Enter <%s>.deleteByFilter(handler: %s, filter: %j, options: %j)', this._modelName, handler, filter, options);

        options = options || {};
        // let user = options.user;

        // 1. check if access is granted
        await this.assertActionGranted(handler, 'delete', options);

        // 2. get repository
        let repo = handler.native.getRepository<A>(this._model);

        // 3. assert `filter` fields
        this.assertValidFields(handler, repo, filter, options);

        // 3.1 assert that assoc is provided in options
        this.assertAssocIncludeOptionsExist(repo, filter, options);

        // 3.2 if assoc exists with an id, make sure that we have an instance with that id in the database.

        if (repo.metadata.ownRelations.length > 0) {

            for (let rel of repo.metadata.ownRelations) {
                let assocName = rel.propertyName;
                let assocModel = rel.type
                let assocRepo = handler.native.getRepository(assocModel);

                if (filter[assocName])
                    await this.assertAssocExists(assocRepo, filter[assocName], options)

            }
        }

        // 3.3. translate filter         
        filter = this.translateFilter(filter)


        // 4. delete `object`
        let result = await repo.createQueryBuilder()
            .delete()
            .where(filter)
            .execute();

        // console.log(result.raw[1])

        return {
            meta: {
                affectedCount: result.raw[1] || 0
            }
        }

    }

    async deleteById(handler: YinzConnection | YinzTransaction, id: number, options?: YinzOptions): Promise<DeleteResult> {
        let result = (await this.deleteByFilter(handler, { id: id } as any, options));

        if (result.meta.affectedCount === 0) {
            throw new Exception('ERR_TYP_ORM_DAO__DLT_BY_ID__INSTANCE_NOT_FOUND', {
                message: `no instance of [${this._modelName}] with id ${id} exists!`
            })
        }

        return result;
    }

    async findAll(handler: YinzConnection | YinzTransaction, options?: YinzOptions & { locale?: string, orderBy?: ModelOrderOp<A> }): Promise<A[]> {
        return await this.findByFilter(handler, {}, options);
    }

    async findByFilter(handler: YinzConnection | YinzTransaction, filter: FilterType<A>, options?: YinzOptions & { locale?: string, orderBy?: ModelOrderOp<A> }): Promise<A[]> {

        this._logger.trace('Enter <%s>.findByFilter(handler: %s, filter: %j, options: %j)', this._modelName, handler, filter, options);

        // let cacheKey: string;

        options = options || {};
        options.include = options.include || [];

        // 1. check if access is granted
        await this.assertActionGranted(handler, 'read', options);

        // 2. get repository
        let repo = handler.native.getRepository<A>(this._model);

        // 4. assert `filter` fields
        this.assertValidFields(handler, repo, filter, options);

        // 5. translate filter         
        filter = this.translateFilter(filter)

        let findOptions = Object.assign({}, {
            where: filter,
            relations: options.include,
            order: options.orderBy,
            cache: true
        })

        // if ( this.enableCaching ) {

        //     cacheKey = this.contrusctCacheKey(this._modelName, findOptions);

        //     let result = await this.cacheManager.getCache(cacheKey);

        //     if ( result && this.isCacheInstanceOfObject(result, repo) ) {
        //         this._logger.warn("will return value found in cache...")                
        //         return result;
        //     }            
        // }

        // 6. fetch rows
        let result = await repo.find(findOptions);

        // 7. add locale if any
        // a better solution would be to contruct the query based on the local  
        // ~~!!~~ PROBLEM, what if we want to get the locale from associate field.
        if (options.locale) {

            // 7.1 check if local model exists


            // 7.1. fetch locale from table            
            try {
                let localeRepo = handler.native.getRepository('I18n' + this._modelName);



                // ignore id, auditTrail and 'locale' fields
                let ignoredFields = ['id', 'lastUpdateBy', 'lastUpdateOn', 'createdBy', 'createdOn', 'locale'];

                for (let row of result) {

                    let localeObj = await localeRepo.findOne({ where: { locale: options.locale, ownerId: row.id } }) as any;

                    if (!localeObj)
                        continue;

                    for (let field in row) {

                        if (ignoredFields.includes(field))
                            continue;

                        if (!!(localeObj[field]))
                            row[field] = localeObj[field];

                    }
                }
            }
            catch (e) { 

                if (e.name === 'RepositoryNotFoundError') {
                    this._logger.warn(`The model [${this._modelName}] doesn't support I18n... will use default model instead.`);
                } else {
                    throw e;
                }

            }

        }

        return result;
    }

    async findById(handler: YinzConnection | YinzTransaction, id: number, options?: YinzOptions): Promise<A> {
        // TODO DO some validation here later and check that we get excatly one instance.
        let result = (await this.findByFilter(handler, { id: id } as any, options));

        if (result.length < 1) {
            throw new Exception('ERR_TYP_ORM_DAO__FND_BY_ID__DATA_NOT_FOUND', {
                message: `no instance of [${this._modelName}] with id ${id} exists!`
            })
        }

        return result[0];
    }

    public async findSingleByFilter(handler: YinzConnection | YinzTransaction, filter: FilterType<A>, options?: YinzOptions): Promise<A | null> {

        let rows = await this.findByFilter(handler, filter, options);

        if (rows.length > 1) {
            throw new Exception('ERR_OUZOUD_TOO_MANY_DATA_FOUND', {
                model: this._modelName,
                filter: filter,
                message: 'There are too many rows within "' + this._modelName + '" for filter "' + JSON.stringify(filter) + '".'
            });
        }

        return rows.length === 0 ? null : rows[0];

    }

    public async save(handler: YinzConnection | YinzTransaction, instance: DeepPartial<A>, options?: YinzOptions & { uniqueKeys?: DeepPartial<A> }): Promise<A> {


        this._logger.trace('Enter <%s>.save(handler: %s, instance: %j, options: %j)', this._modelName, handler, instance, options);

        options = options || {};

        let filter = instance.id ? { id: instance.id } : options.uniqueKeys;

        if (!filter) {
            throw new Exception('ER_COMMONS_DATA_SEQUELIZE__SEQUELIZE_DAO__SAVE__MISS_UNIQUE_KEYS', {
                message: 'The supplied instance "' + JSON.stringify(instance) + '" has no id and there is no unique keys within the supplied options "' +
                    JSON.stringify(options) + '".'
            });
        }

        // we know that filter has a unique key at this point
        filter = filter as DeepPartial<A>;

        // check if instance exist
        let existing = await this.findSingleByFilter(handler, filter, options);

        if (existing) {
            // update if instance exist
            let updateFilter: any = { id: existing.id };
            let updateFields = Object.assign({}, instance, { id: existing.id });
            let result = await this.updateByFilter(handler, updateFields, updateFilter, options);
            return result.data;

        } else {
            // create new one otherwise
            return await this.create(handler, instance, options);
        }


    }

}





