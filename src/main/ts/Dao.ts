import { YinzConnection, YinzTransaction } from "./DataSource";
import { YinzOptions } from "@yinz/commons/ts/YinzOptions";

export interface UpdateResult<A> {
    data: A;
    meta: {
        affectedCount?: number
    }
}

export interface DeleteResult {
    meta: {
        affectedCount: number
    }
}


export default interface Dao<A> {
    
    create(handler: YinzConnection | YinzTransaction, object: any, options?: YinzOptions): Promise<A>;

    createAll(handler: YinzConnection | YinzTransaction, object: any[], options?: YinzOptions): Promise<A[]>;

    updateByFilter(handler: YinzConnection | YinzTransaction, fields: any, filter: any, options?: YinzOptions): Promise<UpdateResult<A>>;

    updateById(handler: YinzConnection | YinzTransaction, fields: any, id: number, options?: YinzOptions): Promise<UpdateResult<A>>;

    deleteByFilter(handler: YinzConnection | YinzTransaction, filter: any, options?: YinzOptions): Promise<DeleteResult>;

    deleteById(handler: YinzConnection | YinzTransaction, id: number, options?: YinzOptions): Promise<DeleteResult>;

    findAll(handler: YinzConnection | YinzTransaction, options?: YinzOptions): Promise<A[]>;

    findByFilter(handler: YinzConnection | YinzTransaction, filter: any, options?: YinzOptions): Promise<A[]>;

    findById(handler: YinzConnection | YinzTransaction, id: number, options?: YinzOptions): Promise<A>

    findSingleByFilter(handler: YinzConnection | YinzTransaction, filter: any, options?: YinzOptions): Promise<A | null>;

    save(handler: YinzConnection | YinzTransaction, instance: any, options?: YinzOptions): Promise<A>;

}