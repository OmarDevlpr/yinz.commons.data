import { Column } from "typeorm";
import Model from "./Model";

export default class LogModel extends Model { 

    /* LOG FIELDS */

    @Column({nullable: true})
    reqRef: string;

    @Column({ nullable: true })
    reqDate: Date;

    @Column({ nullable: true })
    reqSysDate: Date;

    @Column({ nullable: true })
    reqUser: string;

    @Column({ nullable: true })
    reqLoc: string;

    @Column({ nullable: true })
    reqLang: string;

    @Column({ nullable: true })
    tokenCode: string;

    @Column({ nullable: true })
    rspRef: string;

    @Column({ nullable: true })
    rspDate: Date;

    @Column({ nullable: true })
    rspSysDate: Date;

    @Column({ nullable: true })
    rspCode: "A" | "D" | "E";

    @Column({ nullable: true })
    rspReason: string;

    @Column({ nullable: true })
    rspMessage: string;

    @Column({ nullable: true })
    rspExtra: string;
   


};