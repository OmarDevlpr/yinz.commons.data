import { EntityManager, QueryRunner } from "typeorm";
import {YinzOptions} from "@yinz/commons/ts/YinzOptions";

export type YinzConnection = {
    id: string;
    type: string;
    native: EntityManager;
};

export type TypeOrmConnectionOptions = {    
    type: 'postgres' | 'mysql';
    host: string;
    port: number;
    username: string;
    password: string;
    database: string,
    synchronize: boolean;
    cache: any;
};
    
export type YinzTransaction = {
    id: string;
    type: string;
    native: EntityManager;
    queryRunner: QueryRunner;
    conn: YinzConnection;
};

export default interface DataSource {
    
    getConn(options: TypeOrmConnectionOptions): Promise<YinzConnection>;

    relConn(conn: YinzConnection, options?: YinzOptions): Promise<void>;
    
    startTrans(conn: YinzConnection, options?: YinzOptions): Promise<YinzTransaction>;

    commitTrans(trans: YinzTransaction, options?: YinzOptions): Promise<void>;

    rollbackTrans(trans: YinzTransaction, options?: YinzOptions): Promise<void>;
}