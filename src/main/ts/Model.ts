import { Column, PrimaryGeneratedColumn } from "typeorm";

export default class Model {

    /* ID FIELD */
    @PrimaryGeneratedColumn()
    id: number

    /* AUDIT TRAIL FIELDS*/
    @Column({ type: 'timestamptz', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'timestamptz', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;
};