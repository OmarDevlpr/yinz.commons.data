
import DataSource, { TypeOrmConnectionOptions, YinzConnection, YinzTransaction } from "./DataSource";
import { YinzOptions } from "@yinz/commons/ts/YinzOptions";
import { createConnection, Connection, QueryRunner, EntityManager } from "typeorm"
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { Model } from "..";
import { Logger } from "@yinz/commons";

export default class TypeormDataSource implements DataSource {


    private _dbOptions: TypeOrmConnectionOptions;
    private _clazzes: any;
    private _logger: Logger;
    private _queryRunner: QueryRunner;
    private _connection: Connection;    

    constructor(clazzes: any, options: TypeOrmConnectionOptions & { yinzLogger: Logger }) {

        this._dbOptions = options;
        this._clazzes = clazzes;
        this._logger = options.yinzLogger;
    }


    async getConn(options?: TypeOrmConnectionOptions & {yinzLogger?: Logger}): Promise<YinzConnection> {

        // TODO change this later to support all drivers        
        // let clazzes = this._container.getClazzes();
        let entities: Model[] = [];

        if ( this._logger )
            this._logger.debug(`About to register current clazzes >>> \n ${Object.keys(this._clazzes)}`);

        // console.log(this._clazzes)

        for (let clazz in this._clazzes) {            
            if (this._clazzes[clazz].prototype instanceof Model) {
                entities.push(this._clazzes[clazz]);
            }
        }
        // @ts-ignore typescript is going mad here
        const connection: Connection = await createConnection(Object.assign({}, this._dbOptions, { entities }) as PostgresConnectionOptions);
        this._connection = connection;        
        
        this._queryRunner = connection.createQueryRunner();

        await this._queryRunner.connect();

        let conn = {
            id: (Math.random() * (9999999999 - 1000000000) + 1000000000).toFixed(0),
            type: "conn",
            native: this._queryRunner.manager
        }

        return conn;

    }

    public async generateTriggers(): Promise<void> {

        // 1. exlude non supported drivers        
        if ( this._dbOptions.type !== 'postgres') {
            this._logger.warn('only postgres triggers are currently supported... Nothing To Do.')
            return 
        }

        // 2. create notify procedure if it doesn't exist
        await this.createNotifTriggerProcedure(this._queryRunner.manager);

        // 3. create trigger for each registered table
        let tables = this._queryRunner.loadedTables;

        for ( const table of tables ) {

            await this.createTableNotifTrigger(this._queryRunner.manager, table.name);

        }
               
    }

    private async createTableNotifTrigger(entityManager: EntityManager, tableName: string): Promise<void> {

        const sql =
            `
                CREATE TRIGGER ${tableName}_notify AFTER INSERT OR UPDATE OR DELETE ON ${tableName}
                FOR EACH STATEMENT EXECUTE PROCEDURE notify_trigger();                            
            `

        await entityManager.query(sql);

    }

    private async createNotifTriggerProcedure(entityManager: EntityManager): Promise<void> {

        const sql = `
        CREATE OR REPLACE FUNCTION notify_trigger()
            RETURNS trigger AS
        $func$
        DECLARE
            payload TEXT;
        BEGIN
            payload := '{ "table":"' || TG_TABLE_NAME || '"}';
            PERFORM pg_notify('db_notifications', payload);
            RETURN NULL;
        END
        $func$  LANGUAGE plpgsql;`

        await entityManager.query(sql);

    }

    async relConn(conn: YinzConnection, options?: YinzOptions): Promise<void> {              
        await this._queryRunner.release();                   
        await this._connection.close()     
    }

    async startTrans(conn: YinzConnection, options?: YinzOptions): Promise<YinzTransaction> {

        let queryRunner = this._connection.createQueryRunner();

        await queryRunner.startTransaction();

        let trans = {
            id: (Math.random() * (9999999999 - 1000000000) + 1000000000).toFixed(0),
            type: "trans",
            native: queryRunner.manager,
            queryRunner,
            conn: conn
        }

        return trans;

    }
    async commitTrans(trans: YinzTransaction, options?: YinzOptions): Promise<void> {

        await trans.queryRunner.commitTransaction();
        await trans.queryRunner.release();
        // await this._queryRunner.commitTransaction();      
        // await this._queryRunner.release();    
    }

    async rollbackTrans(trans: YinzTransaction, options?: YinzOptions): Promise<void> {

        await trans.queryRunner.rollbackTransaction();
        await trans.queryRunner.release();
        // await this._queryRunner.rollbackTransaction();      
        // await this._queryRunner.release();  
    }
}