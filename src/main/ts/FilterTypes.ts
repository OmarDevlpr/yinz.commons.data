export interface EqualFilter {
    op: "=" /*| "!="*/;
    val: string | boolean | number;
}

export interface CompareFilter {
    op: "<" | "<=" | ">" | ">=";
    val: number | Date;
}

export interface LikeFilter {
    op: "LIKE" | "like";
    val: string;
}
export interface BetweenFilter {
    op: "BETWEEN" | "between";
    val: [number, number];
}

export interface InFilter {
    op: "IN" | "in";
    val: string[] | number[];
}

export type ArrayMinTwoElem<T> = {
    0: T
    1: T
} & Array<T>

export type WhereFilter = CompareFilter | LikeFilter | BetweenFilter | InFilter | EqualFilter;

// export type valueof<T> = T[keyof T];

export type ModelFilterOp<T> = {
    [P in keyof T]?: WhereFilter;
}

export type ModelOrderOp<T> = {
    [P in keyof T]?: "ASC" | "DESC";
}
