
import Container from "@yinz/container/ts/Container";
import TypeormDataSource from "./TypeormDataSource";
import TypeormDao from "./TypeormDao";
import Model from "./Model";
import { Logger } from "@yinz/commons";
// import RedisCacheManager from "@yinz/cache.services/ts/RedisCacheManager";


const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>('logger')


    let typeormDataSource = new TypeormDataSource(container.getClazzes(), {...container.getParam('db'), yinzLogger: logger});

    container.setBean('typeormDataSource', typeormDataSource);

};

const registerClazzes = (container: Container) => {

    // let dbParm = container.getParam("db");

    // if ( dbParm["cache"] && dbParm["cache"]["enable"]) {

    //     TypeormDao.prototype.enableCaching = dbParm["cache"]["enable"];
    //     TypeormDao.prototype.cacheManager  = container.getBean<RedisCacheManager>("redisCacheManager");
    // }

    

    container.setClazz('TypeormDataSource', TypeormDataSource);
    container.setClazz('TypeormDao', TypeormDao);
    container.setClazz('Model', Model);        

};


export {
    registerBeans,
    registerClazzes
};